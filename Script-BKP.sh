#!/bin/bash

# Variables
DB_USER="backup"
DB_PASS="closeddoor1234*"
DB_NAME="seahawk63"
BACKUP_DIR="/var/Backup/BDD"
LOG_FILE="/var/Backup/BDD/LOGS/backup.log"  # Chemin vers le fichier de logs
DATE=$(date +%Y%m%d_%H%M%S)
BACKUP_FILE="$BACKUP_DIR/$DB_NAME-$DATE.sql"


# Création du répertoire de sauvegarde s'il n'existe pas
mkdir -p $BACKUP_DIR
mkdir -p $BACKUP_DIR/LOGS


# Export de la base de données avec journalisation dans le fichier de logs
echo "$(date +'%Y-%m-%d %H:%M:%S') - Début de la sauvegarde de la base de données $DB_NAME" >> $LOG_FILE
mysqldump -u$DB_USER -p$DB_PASS $DB_NAME > $BACKUP_FILE 2>> $LOG_FILE

# Vérification de la réussite de l'export
if [ $? -eq 0 ]; then
  echo "$(date +'%Y-%m-%d %H:%M:%S') - Sauvegarde de la base de données $DB_NAME réussie : $BACKUP_FILE" >> $LOG_FILE
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') - Erreur lors de la sauvegarde de la base de données $DB_NAME. Voir le fichier de logs pour plus de détails." >> $LOG_FILE
fi

# Suppression des fichiers de sauvegarde plus anciens qu'une semaine
find $BACKUP_DIR -name "$DB_NAME-*.sql" -type f -mtime +7 -exec rm {} \;
if [ $? -eq 0 ]; then
  echo "$(date +'%Y-%m-%d %H:%M:%S') - Suppression des anciennes sauvegardes réussie" >> $LOG_FILE
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') - Erreur lors de la suppression des anciennes sauvegardes. Voir le fichier de logs pour plus de détails." >> $LOG_FILE
fi
