-- Liste des instances d’un client prestataire donné

DELIMITER //

CREATE PROCEDURE ListInstancesByPrestataire(IN prestataire_name VARCHAR(255))
BEGIN
    SELECT s.*
    FROM sonde s
    JOIN client c ON s.ID_Client = c.ID_Client
    JOIN prestataire p ON c.ID_prestataire = p.ID_prestataire
    WHERE p.nom_prestataire = prestataire_name;
END //

DELIMITER ;


-- Liste des incidents pour une instance donnée


DELIMITER //

CREATE PROCEDURE ListIncidentsByInstance(IN instance_name VARCHAR(255))
BEGIN
    SELECT i.*
    FROM incident i
    JOIN sonde s ON i.ID_Sonde = s.ID_Sonde
    WHERE s.nom_sonde = instance_name;
END //

DELIMITER ;


-- Liste des instances qui n’ont jamais été redémarrées manuellement


DELIMITER //

CREATE PROCEDURE ListInstancesNeverRestartedManually()
BEGIN
    SELECT s.*
    FROM sonde s
    LEFT JOIN historique_red hr ON s.ID_Sonde = hr.ID_sonde AND hr.motif = 'Maintenance régulière'
    WHERE hr.ID_Hist IS NULL;
END //

DELIMITER ;



-- Liste des instances d’un client prestataire donné qui doivent être récupérées sur site



DELIMITER //

CREATE PROCEDURE ListInstancesToBeRetrieved(IN prestataire_name VARCHAR(255))
BEGIN
    SELECT s.*
    FROM sonde s
    JOIN client c ON s.ID_Client = c.ID_Client
    JOIN prestataire p ON c.ID_prestataire = p.ID_prestataire
    WHERE p.nom_prestataire = prestataire_name
    AND DATE_ADD(s.dlc, INTERVAL 1 YEAR) < CURDATE(); -- suppose une durée de contrat de 1 an
END //

DELIMITER ;

-- Historique des installations pour une instance donnée



DELIMITER //

CREATE PROCEDURE InstallationHistoryByInstance(IN instance_name VARCHAR(255))
BEGIN
    SELECT s.nom_sonde, u.nom_user AS technicien, c.nom_client AS client_final, s.dlc AS date_installation, 
           DATE_ADD(s.dlc, INTERVAL 1 YEAR) AS date_fin_contrat -- suppose une durée de contrat de 1 an
    FROM sonde s
    JOIN client c ON s.ID_Client = c.ID_Client
    JOIN users u ON c.ID_prestataire = u.ID_prestataire
    WHERE s.nom_sonde = instance_name;
END //

DELIMITER ;


-- procédure stockée pour lister les instances redémarrées manuellement plus de 5 fois



DELIMITER //

CREATE PROCEDURE ListInstancesRestartedManually()
BEGIN
    SELECT s.*
    FROM sonde s
    JOIN (
        SELECT ID_sonde, COUNT(*) AS restart_count
        FROM historique_red
        WHERE motif = 'Maintenance régulière'
        GROUP BY ID_sonde
        HAVING restart_count > 5
    ) hr ON s.ID_Sonde = hr.ID_sonde;
END //

DELIMITER ;

CALL ListInstancesByPrestataire('NFL IT');
CALL ListIncidentsByInstance('Sonde Epsilon Hardware');
CALL ListInstancesNeverRestartedManually();
CALL ListInstancesToBeRetrieved('NFL IT');
CALL InstallationHistoryByInstance('Sonde NFL KC');
CALL ListInstancesRestartedManually();