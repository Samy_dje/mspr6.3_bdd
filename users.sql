CREATE USER 'admin_seahawk'@'%' IDENTIFIED BY 'Adm1n$User_Str0ngPass!';
CREATE USER 'readonly_seahawk'@'%' IDENTIFIED BY 'Read0nly$User_Str0ngPass!';
CREATE USER 'readwrite_seahawk'@'%' IDENTIFIED BY 'ReadWr1t3$User_Str0ngPass!';

GRANT ALL PRIVILEGES ON `seahawk63`.* TO 'admin_seahawk'@'%';

GRANT SELECT ON `seahawk63`.* TO 'readonly_seahawk'@'%';

GRANT SELECT, INSERT, UPDATE, DELETE ON `seahawk63`.* TO 'readwrite_seahawk'@'%';

FLUSH PRIVILEGES;
