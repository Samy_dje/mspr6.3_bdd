-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           11.3.2-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour seahawk63
CREATE DATABASE IF NOT EXISTS `seahawk63` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci */;
USE `seahawk63`;

-- Listage de la structure de la table seahawk63. client
CREATE TABLE IF NOT EXISTS `client` (
  `ID_Client` int(11) NOT NULL AUTO_INCREMENT,
  `nom_client` varchar(255) NOT NULL,
  `adresse_client` varchar(255) DEFAULT NULL,
  `code_postal` varchar(10) DEFAULT NULL,
  `ville_client` varchar(255) DEFAULT NULL,
  `ID_prestataire` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_Client`),
  KEY `ID_prestataire` (`ID_prestataire`),
  KEY `index_nom_client_client` (`nom_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=11;

-- Listage de la structure de la table seahawk63. historique_red
CREATE TABLE IF NOT EXISTS `historique_red` (
  `ID_Hist` int(11) NOT NULL AUTO_INCREMENT,
  `motif` text DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `ID_sonde` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_Hist`),
  KEY `ID_sonde` (`ID_sonde`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Listage de la structure de la table seahawk63. incident
CREATE TABLE IF NOT EXISTS `incident` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `description` text DEFAULT NULL,
  `date_incident` date DEFAULT NULL,
  `ID_Sonde` int(11) DEFAULT NULL,
  `ID_User` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID_Sonde` (`ID_Sonde`),
  KEY `ID_User` (`ID_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;


-- Listage de la structure de la table seahawk63. prestataire
CREATE TABLE IF NOT EXISTS `prestataire` (
  `ID_prestataire` int(11) NOT NULL AUTO_INCREMENT,
  `nom_prestataire` varchar(255) NOT NULL,
  `siret` varchar(14) DEFAULT NULL,
  `adresse_prestataire` varchar(255) DEFAULT NULL,
  `code_postal` varchar(10) DEFAULT NULL,
  `ville_prestataire` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_prestataire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=6;


-- Listage de la structure de la table seahawk63. ressource
CREATE TABLE IF NOT EXISTS `ressource` (
  `ID_Ressource` int(11) NOT NULL AUTO_INCREMENT,
  `cpu` varchar(255) DEFAULT NULL,
  `ram` int(11) DEFAULT NULL,
  `disque` int(11) DEFAULT NULL,
  `taille_disque` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ID_Ressource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Listage de la structure de la table seahawk63. scan_reseaux
CREATE TABLE IF NOT EXISTS `scan_reseaux` (
  `Scan_ID` int(11) NOT NULL AUTO_INCREMENT,
  `adresse_ip` varchar(15) DEFAULT NULL,
  `ports_ouverts` tinyint(1) DEFAULT NULL,
  `nom_machine` varchar(255) DEFAULT NULL,
  `date_scan` date DEFAULT NULL,
  `ID_sonde` int(11) DEFAULT NULL,
  PRIMARY KEY (`Scan_ID`),
  KEY `ID_sonde` (`ID_sonde`),
  KEY `index_adresse_ip_scan_reseaux` (`adresse_ip`),
  KEY `index_ports_ouverts_scan_reseaux` (`ports_ouverts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Listage de la structure de la table seahawk63. sonde
CREATE TABLE IF NOT EXISTS `sonde` (
  `ID_Sonde` int(11) NOT NULL AUTO_INCREMENT,
  `nom_sonde` varchar(255) NOT NULL,
  `numero_serie` varchar(255) DEFAULT NULL,
  `numero_licence` varchar(255) DEFAULT NULL,
  `etat` varchar(50) DEFAULT NULL,
  `dlc` date DEFAULT NULL,
  `version_harvester` varchar(50) DEFAULT NULL,
  `scripts` varchar(50) DEFAULT NULL,
  `ID_Client` int(11) DEFAULT NULL,
  `ID_Ressource` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_Sonde`),
  KEY `ID_Client` (`ID_Client`),
  KEY `ID_Ressource` (`ID_Ressource`),
  KEY `index_nom_sonde_sonde` (`nom_sonde`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=11;


-- Listage de la structure de la table seahawk63. users
CREATE TABLE IF NOT EXISTS `users` (
  `ID_User` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nom_user` varchar(255) DEFAULT NULL,
  `prenom_user` varchar(255) DEFAULT NULL,
  `numero_tel` varchar(15) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `ID_prestataire` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_User`),
  KEY `ID_prestataire` (`ID_prestataire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;


-- Add foreign key constraints
ALTER TABLE `client` ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`ID_prestataire`) REFERENCES `prestataire` (`ID_prestataire`);
ALTER TABLE `historique_red` ADD CONSTRAINT `historique_red_ibfk_1` FOREIGN KEY (`ID_sonde`) REFERENCES `sonde` (`ID_Sonde`);
ALTER TABLE `incident` ADD CONSTRAINT `incident_ibfk_1` FOREIGN KEY (`ID_Sonde`) REFERENCES `sonde` (`ID_Sonde`);
ALTER TABLE `incident` ADD CONSTRAINT `incident_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `users` (`ID_User`);
ALTER TABLE `scan_reseaux` ADD CONSTRAINT `scan_reseaux_ibfk_1` FOREIGN KEY (`ID_sonde`) REFERENCES `sonde` (`ID_Sonde`);
ALTER TABLE `sonde` ADD CONSTRAINT `sonde_ibfk_1` FOREIGN KEY (`ID_Client`) REFERENCES `client` (`ID_Client`);
ALTER TABLE `sonde` ADD CONSTRAINT `sonde_ibfk_2` FOREIGN KEY (`ID_Ressource`) REFERENCES `ressource` (`ID_Ressource`);
ALTER TABLE `users` ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ID_prestataire`) REFERENCES `prestataire` (`ID_prestataire`);

INSERT INTO `prestataire` (`ID_prestataire`, `nom_prestataire`, `siret`, `adresse_prestataire`, `code_postal`, `ville_prestataire`, `contact`) VALUES
(1, 'NFL IT', '78945612300125', 'One NFL Plaza, Kansas City', '64101', 'Missouri', 'info@nflit.com'),
(2, 'Tech Dynamics', '45678912300126', 'Tech Park, Silicon Valley', '94025', 'California', 'support@techdynamics.com'),
(3, 'DataSecure', '12378945600127', 'Data Blvd, New York', '10001', 'New York', 'contact@datasecure.com'),
(4, 'CloudNet Solutions', '78912345600128', 'Cloud St, Seattle', '98101', 'Washington', 'help@cloudnetsol.com'),
(5, 'InnovaTech', '45612378900129', 'Innovation Way, Boston', '02101', 'Massachusetts', 'service@innovatech.com');

INSERT INTO `client` (`ID_Client`, `nom_client`, `adresse_client`, `code_postal`, `ville_client`, `ID_prestataire`) VALUES
(1, 'Kansas Chiefs', 'Arrowhead Dr, Kansas City', '64129', 'Missouri', 1),
(2, 'Miami Dolphins', 'Dolphin Road, Miami', '33101', 'Florida', 1),
(3, 'Silicon Valley Tech Giants', 'Tech Park, Silicon Valley', '94025', 'California', 2),
(4, 'NY Data Corp', 'Wall St, New York', '10005', 'New York', 2),
(5, 'Seattle IT Hub', 'Pine St, Seattle', '98101', 'Washington', 3),
(6, 'Washington Tech', 'Capitol St, Washington', '20001', 'Washington D.C.', 3),
(7, 'Boston Innovators', 'Innovation Way, Boston', '02210', 'Massachusetts', 4),
(8, 'Cambridge Techies', 'Tech Sq, Cambridge', '02139', 'Massachusetts', 4),
(9, 'LA Tech Network', 'Hollywood Blvd, Los Angeles', '90028', 'California', 5),
(10, 'San Diego Systems', 'Sunset Cliffs, San Diego', '92107', 'California', 5);

INSERT INTO `ressource` (`ID_Ressource`, `cpu`, `ram`, `disque`, `taille_disque`, `os`, `ip`) VALUES
(1, 'Intel Xeon', 32, 1, '1TB', 'Windows Server 2019', '192.168.1.100'),
(2, 'AMD Ryzen', 16, 1, '500GB', 'Ubuntu 20.04', '192.168.1.101'),
(3, 'Intel i7', 16, 2, '256GB', 'Windows 10', '192.168.1.102'),
(4, 'Intel Xeon', 64, 4, '4TB', 'CentOS 8', '192.168.1.103'),
(5, 'AMD EPYC', 64, 2, '2TB', 'Debian 10', '192.168.1.104');

INSERT INTO `sonde` (`ID_Sonde`, `nom_sonde`, `numero_serie`, `numero_licence`, `etat`, `dlc`, `version_harvester`, `ID_Client`, `ID_Ressource`) VALUES
(1, 'Sonde NFL KC', 'VNS-NFLKC', 'Lic-001', 'Active', '2025-12-31', NULL, 1, 1),
(2, 'Sonde NFL MIA', 'VNS-NFLMIA', 'Lic-002', 'Active', '2025-12-31', NULL, 2, 2),
(3, 'Sonde Tech Giants', 'VNS-TechG', 'Lic-003', 'Active', '2025-12-31', NULL, 3, 3),
(4, 'Sonde NY Data', 'VNS-NYD', 'Lic-004', 'Active', '2025-12-31', NULL, 4, 4),
(5, 'Sonde Alpha Tech', 'VNS-Alpha', 'Lic-005', 'Active', '2025-12-31', NULL, 5, 5),
(6, 'Sonde Beta Systems', 'VNS-Beta', 'Lic-006', 'Active', '2025-12-31', NULL, 6, NULL),
(7, 'Sonde Gamma Support', 'VNS-Gamma', 'Lic-007', 'Active', '2025-12-31', NULL, 7, NULL),
(8, 'Sonde Delta Services', 'VNS-Delta', 'Lic-008', 'Active', '2025-12-31', NULL, 8, NULL),
(9, 'Sonde Epsilon Hardware', 'VNS-Epsilon', 'Lic-009', 'Active', '2025-12-31', NULL, 9, NULL),
(10, 'Sonde Zeta Solutions', 'VNS-Zeta', 'Lic-010', 'Active', '2025-12-31', NULL, 10, NULL);

INSERT INTO `users` (`ID_User`, `username`, `password`, `nom_user`, `prenom_user`, `numero_tel`, `role`, `ID_prestataire`) VALUES
(1, 'tech1', 'password1', 'Johnson', 'Bill', '8165550123', 'Technician', 1),
(2, 'tech2', 'password2', 'Martin', 'Chris', '8165550456', 'Technician', 1);

INSERT INTO `historique_red` (`ID_Hist`, `motif`, `timestamp`, `ID_sonde`) VALUES
(1, 'Maintenance régulière', NOW(), 1),
(2, 'Mise à jour logicielle', NOW(), 2),
(3, 'Vérification de performance', NOW(), 3),
(4, 'Remplacement de composant', NOW(), 4),
(5, 'Audit de sécurité', NOW(), 5);

INSERT INTO `incident` (`ID`, `description`, `date_incident`, `ID_Sonde`, `ID_User`) VALUES
(1, 'Problème de connexion', CURDATE(), 10, 1),
(2, 'Erreur de licence', CURDATE(), 9, 2);


INSERT INTO `scan_reseaux` (`Scan_ID`, `adresse_ip`, `ports_ouverts`, `nom_machine`, `date_scan`, `ID_sonde`) VALUES
(1, '192.168.1.100', 1, 'Server-1', CURDATE(), 1),
(2, '192.168.1.101', 0, 'Server-2', CURDATE(), 2),
(3, '192.168.1.102', 1, 'Workstation-1', CURDATE(), 3),
(4, '192.168.1.103', 1, 'Database-Server', CURDATE(), 4),
(5, '192.168.1.104', 0, 'Web-Server', CURDATE(), 5);

-- Listage de la structure de la vue seahawk63. vue_client_par_prestataire
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_client_par_prestataire` (
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_client_pour_cloudnet_solutions
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_client_pour_cloudnet_solutions` (
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`adresse_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`code_postal` VARCHAR(10) NULL COLLATE 'utf8mb3_general_ci',
	`ville_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_client_pour_datasecure
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_client_pour_datasecure` (
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`adresse_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`code_postal` VARCHAR(10) NULL COLLATE 'utf8mb3_general_ci',
	`ville_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_client_pour_innovatech
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_client_pour_innovatech` (
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`adresse_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`code_postal` VARCHAR(10) NULL COLLATE 'utf8mb3_general_ci',
	`ville_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_client_pour_nfl_it
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_client_pour_nfl_it` (
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`adresse_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`code_postal` VARCHAR(10) NULL COLLATE 'utf8mb3_general_ci',
	`ville_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_client_pour_tech_dynamics
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_client_pour_tech_dynamics` (
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`adresse_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`code_postal` VARCHAR(10) NULL COLLATE 'utf8mb3_general_ci',
	`ville_client` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_sondes_par_prestataire
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_sondes_par_prestataire` (
	`ID_Sonde` INT(11) NOT NULL,
	`nom_sonde` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_sondes_pour_cloudnet_solutions
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_sondes_pour_cloudnet_solutions` (
	`ID_Sonde` INT(11) NOT NULL,
	`nom_sonde` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`numero_serie` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`numero_licence` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`etat` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`dlc` DATE NULL,
	`version_harvester` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`scripts` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_sondes_pour_datasecure
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_sondes_pour_datasecure` (
	`ID_Sonde` INT(11) NOT NULL,
	`nom_sonde` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`numero_serie` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`numero_licence` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`etat` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`dlc` DATE NULL,
	`version_harvester` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`scripts` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_sondes_pour_innovatech
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_sondes_pour_innovatech` (
	`ID_Sonde` INT(11) NOT NULL,
	`nom_sonde` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`numero_serie` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`numero_licence` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`etat` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`dlc` DATE NULL,
	`version_harvester` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`scripts` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_sondes_pour_nfl_it
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_sondes_pour_nfl_it` (
	`ID_Sonde` INT(11) NOT NULL,
	`nom_sonde` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`numero_serie` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`numero_licence` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`etat` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`dlc` DATE NULL,
	`version_harvester` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`scripts` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Listage de la structure de la vue seahawk63. vue_sondes_pour_tech_dynamics
-- Création d'une table temporaire pour palier aux erreurs de dépendances de VIEW
CREATE TABLE `vue_sondes_pour_tech_dynamics` (
	`ID_Sonde` INT(11) NOT NULL,
	`nom_sonde` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_Client` INT(11) NOT NULL,
	`nom_client` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`ID_prestataire` INT(11) NOT NULL,
	`nom_prestataire` VARCHAR(255) NOT NULL COLLATE 'utf8mb3_general_ci',
	`numero_serie` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`numero_licence` VARCHAR(255) NULL COLLATE 'utf8mb3_general_ci',
	`etat` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`dlc` DATE NULL,
	`version_harvester` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci',
	`scripts` VARCHAR(50) NULL COLLATE 'utf8mb3_general_ci'
) ENGINE=MyISAM;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_client_par_prestataire`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_client_par_prestataire` AS SELECT 
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire
FROM 
    client c 
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_client_pour_cloudnet_solutions`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_client_pour_cloudnet_solutions` AS SELECT 
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    c.adresse_client,
    c.code_postal,
    c.ville_client
FROM 
    client c 
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'CloudNet Solutions' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_client_pour_datasecure`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_client_pour_datasecure` AS SELECT 
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    c.adresse_client,
    c.code_postal,
    c.ville_client
FROM 
    client c 
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'DataSecure' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_client_pour_innovatech`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_client_pour_innovatech` AS SELECT 
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    c.adresse_client,
    c.code_postal,
    c.ville_client
FROM 
    client c 
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'InnovaTech' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_client_pour_nfl_it`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_client_pour_nfl_it` AS SELECT 
   c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    c.adresse_client,
    c.code_postal,
    c.ville_client
FROM 
    client c 
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'NFL IT' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_client_pour_tech_dynamics`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_client_pour_tech_dynamics` AS SELECT 
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    c.adresse_client,
    c.code_postal,
    c.ville_client
FROM 
    client c 
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'Tech Dynamics' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_sondes_par_prestataire`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_sondes_par_prestataire` AS SELECT 
    s.ID_Sonde AS ID_Sonde,
    s.nom_sonde,
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire
FROM 
    sonde s
JOIN 
    client c ON s.ID_Client = c.ID_Client
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_sondes_pour_cloudnet_solutions`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_sondes_pour_cloudnet_solutions` AS SELECT 
    s.ID_Sonde AS ID_Sonde,
    s.nom_sonde,
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    numero_serie,
    numero_licence,
    etat,
    dlc,
    version_harvester,
    scripts
FROM 
    sonde s
JOIN 
    client c ON s.ID_Client = c.ID_Client
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'CloudNet Solutions' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_sondes_pour_datasecure`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_sondes_pour_datasecure` AS SELECT 
    s.ID_Sonde AS ID_Sonde,
    s.nom_sonde,
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    numero_serie,
    numero_licence,
    etat,
    dlc,
    version_harvester,
    scripts
FROM 
    sonde s
JOIN 
    client c ON s.ID_Client = c.ID_Client
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'DataSecure' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_sondes_pour_innovatech`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_sondes_pour_innovatech` AS SELECT 
    s.ID_Sonde AS ID_Sonde,
    s.nom_sonde,
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    numero_serie,
    numero_licence,
    etat,
    dlc,
    version_harvester,
    scripts
FROM 
    sonde s
JOIN 
    client c ON s.ID_Client = c.ID_Client
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'InnovaTech' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_sondes_pour_nfl_it`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_sondes_pour_nfl_it` AS SELECT 
    s.ID_Sonde AS ID_Sonde,
    s.nom_sonde,
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    numero_serie,
    numero_licence,
    etat,
    dlc,
    version_harvester,
    scripts
FROM 
    sonde s
JOIN 
    client c ON s.ID_Client = c.ID_Client
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'NFL IT' ;

-- Suppression de la table temporaire et création finale de la structure de VIEW
DROP TABLE IF EXISTS `vue_sondes_pour_tech_dynamics`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vue_sondes_pour_tech_dynamics` AS SELECT 
    s.ID_Sonde AS ID_Sonde,
    s.nom_sonde,
    c.ID_Client AS ID_Client,
    c.nom_client,
    p.ID_prestataire AS ID_prestataire,
    p.nom_prestataire,
    numero_serie,
    numero_licence,
    etat,
    dlc,
    version_harvester,
    scripts
FROM 
    sonde s
JOIN 
    client c ON s.ID_Client = c.ID_Client
JOIN 
    prestataire p ON c.ID_prestataire = p.ID_prestataire
WHERE 
	nom_prestataire = 'Tech Dynamics' ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
